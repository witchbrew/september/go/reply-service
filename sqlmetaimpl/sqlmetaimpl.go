package sqlmetaimpl

import (
	coreSqlMeta "gitlab.com/witchbrew/go/sqlmeta"
	"gitlab.com/witchbrew/september/go/reply-service/sqlmeta"
)

var AnswerTable = &sqlmeta.AnswerTable{
	Table: coreSqlMeta.NewTable("answers"),
	Columns: &sqlmeta.AnswerColumns{
		ID:         "id",
		QuestionID: "question_id",
	},
}

var OpenEndedAnswerTable = &sqlmeta.OpenEndedAnswerTable{
	Table:   coreSqlMeta.NewTable("open_ended_answers"),
	Columns: &sqlmeta.OpenEndedAnswerColumns{
		AnswerID: "answer_id",
		Text:     "text",
	},
}

var CloseEndedAnswerTable = &sqlmeta.CloseEndedAnswerTable{
	Table:   coreSqlMeta.NewTable("close_ended_answers"),
	Columns: &sqlmeta.CloseEndedAnswerColumns{
		AnswerID:    "answer_id",
		OptionIndex: "option_index",
	},
}