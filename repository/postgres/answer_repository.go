package postgres

import (
	"database/sql"
	"github.com/pkg/errors"
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/go/postgresutils"
	"gitlab.com/witchbrew/go/sqltemplate"
	"gitlab.com/witchbrew/go/sqlutils"
	"gitlab.com/witchbrew/september/go/reply-service/model/answer"
	"gitlab.com/witchbrew/september/go/reply-service/repository"
	"gitlab.com/witchbrew/september/go/reply-service/sqlmeta"
	"gitlab.com/witchbrew/september/go/reply-service/sqlmetaimpl"
)

type answerRepository struct {
	db                    *sql.DB
	answerTable           *sqlmeta.AnswerTable
	openEndedAnswerTable  *sqlmeta.OpenEndedAnswerTable
	closeEndedAnswerTable *sqlmeta.CloseEndedAnswerTable
}

const closeEndedAnswerColumnsNum = 2

var insertAnswerT = sqltemplate.New(`
INSERT INTO {{t}} ({{qIDC}}) VALUES ($1) RETURNING {{idC}}
`)

var insertOpenEndedAnswerT = sqltemplate.New(`
INSERT INTO {{t}} ({{aIDC}}, {{textC}}) VALUES ($1, $2)
`)

var insertCloseEndedAnswerT = sqltemplate.New(`
INSERT INTO {{t}} ({{aIDC}}, {{oiC}}) VALUES {{values}}
`)

func (r *answerRepository) Create(a *answer.Answer) error {
	if a.ID().IsInitialized() {
		return errors.Errorf("answer ID is already initialized to %v", a.ID().Value)
	}
	tx, err := r.db.Begin()
	if err != nil {
		return errors.Wrap(err, "failed to start transaction")
	}
	answerQuery := insertAnswerT.Render(sqltemplate.Params{
		"t":    r.answerTable.Name,
		"qIDC": r.answerTable.Columns.QuestionID,
		"idC":  r.answerTable.Columns.ID,
	})
	var aIDVal uint64
	err = tx.QueryRow(answerQuery, a.QuestionID()).Scan(&aIDVal)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to scan answer ID")
	}
	if a.HasOptions() {
		closeEndedAnswerQuery := insertCloseEndedAnswerT.Render(sqltemplate.Params{
			"t":      r.closeEndedAnswerTable.Name,
			"aIDC":   r.closeEndedAnswerTable.Columns.AnswerID,
			"oiC":    r.closeEndedAnswerTable.Columns.OptionIndex,
			"values": postgresutils.InsertValues(closeEndedAnswerColumnsNum, len(a.Options())),
		})
		optionsIndexesValues := make([]interface{}, closeEndedAnswerColumnsNum*len(a.Options()))
		for index, optionIndex := range a.Options() {
			optionsIndexesValues[closeEndedAnswerColumnsNum*index] = aIDVal
			optionsIndexesValues[closeEndedAnswerColumnsNum*index+1] = optionIndex
		}
		_, err = tx.Exec(closeEndedAnswerQuery, optionsIndexesValues...)
		if err != nil {
			return sqlutils.RollbackTxErrWrap(tx, err, "failed to insert close ended answer")
		}
	} else {
		openEndedAnswerQuery := insertOpenEndedAnswerT.Render(sqltemplate.Params{
			"t":     r.openEndedAnswerTable.Name,
			"aIDC":  r.openEndedAnswerTable.Columns.AnswerID,
			"textC": r.openEndedAnswerTable.Columns.Text,
		})
		_, err = tx.Exec(openEndedAnswerQuery, aIDVal, a.Text())
		if err != nil {
			return sqlutils.RollbackTxErrWrap(tx, err, "failed to insert open ended answer")
		}
	}
	err = tx.Commit()
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to commit transaction")
	}
	a.ID().Value = aIDVal
	return nil
}

var selectOpenEndedAnswerT = sqltemplate.New(`
SELECT {{aT}}.{{qIDC}}, {{oeaT}}.{{textC}}
FROM {{aT}}
LEFT JOIN {{oeaT}} ON {{aT}}.{{idC}}={{oeaT}}.{{aIDC}}
WHERE {{aT}}.{{idC}}=$1
`)

var selectCloseEndedAnswerT = sqltemplate.New(`
SELECT {{oiC}} FROM {{t}} WHERE {{aIDC}}=$1
`)

func (r *answerRepository) Get(i *id.ID) (*answer.Answer, error) {
	openEndedAnswerQuery := selectOpenEndedAnswerT.Render(sqltemplate.Params{
		"aT":    r.answerTable.Name,
		"qIDC":  r.answerTable.Columns.QuestionID,
		"oeaT":  r.openEndedAnswerTable.Name,
		"textC": r.openEndedAnswerTable.Columns.Text,
		"idC":   r.answerTable.Columns.ID,
		"aIDC":  r.openEndedAnswerTable.Columns.AnswerID,
	})
	// Pointer as it can be NULL in the DB
	var questionID string
	var openEndedAnswerText *string
	err := r.db.QueryRow(openEndedAnswerQuery, i.Value).Scan(&questionID, &openEndedAnswerText)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, errors.Wrap(err, "failed to scan open ended answer")
	}
	var a *answer.Answer
	if openEndedAnswerText == nil {
		closeEndedAnswerQuery := selectCloseEndedAnswerT.Render(sqltemplate.Params{
			"oiC":  r.closeEndedAnswerTable.Columns.OptionIndex,
			"t":    r.closeEndedAnswerTable.Name,
			"aIDC": r.closeEndedAnswerTable.Columns.AnswerID,
		})
		rows, err := r.db.Query(closeEndedAnswerQuery, i.Value)
		if err != nil {
			return nil, errors.Wrap(err, "failed to query close ended answer")
		}
		var options []int
		for rows.Next() {
			var option int
			err = rows.Scan(&option)
			if err != nil {
				return nil, sqlutils.CloseRowsErrWrap(rows, err, "failed to scan option index")
			}
			options = append(options, option)
		}
		err = rows.Close()
		if err != nil {
			return nil, errors.Wrap(err, "failed to close close ended answer rows")
		}
		a = answer.CloseEnded(questionID, options...)
	} else {
		a = answer.OpenEnded(questionID, *openEndedAnswerText)
	}
	a.ID().Value = i.Value
	return a, nil
}

var deleteT = sqltemplate.New(`
DELETE FROM {{t}} WHERE {{idC}}=$1
`)

func (r *answerRepository) Delete(i *id.ID) error {
	if !i.IsInitialized() {
		return id.ErrNotInitialized
	}
	tx, err := r.db.Begin()
	if err != nil {
		return errors.Wrap(err, "failed to start transaction")
	}
	closeEndedAnswerQuery := deleteT.Render(sqltemplate.Params{
		"t":   r.closeEndedAnswerTable.Name,
		"idC": r.closeEndedAnswerTable.Columns.AnswerID,
	})
	_, err = tx.Exec(closeEndedAnswerQuery, i.Value)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to delete close ended answer")
	}
	openEndedAnswerQuery := deleteT.Render(sqltemplate.Params{
		"t":   r.openEndedAnswerTable.Name,
		"idC": r.openEndedAnswerTable.Columns.AnswerID,
	})
	_, err = tx.Exec(openEndedAnswerQuery, i.Value)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to delete open ended answer")
	}
	answerQuery := deleteT.Render(sqltemplate.Params{
		"t":   r.answerTable.Name,
		"idC": r.answerTable.Columns.ID,
	})
	_, err = tx.Exec(answerQuery, i.Value)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to delete answer")
	}
	err = tx.Commit()
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to commit transaction")
	}
	return nil
}

func NewAnswerRepository(db *sql.DB) repository.Answer {
	return &answerRepository{
		db:                    db,
		answerTable:           sqlmetaimpl.AnswerTable,
		openEndedAnswerTable:  sqlmetaimpl.OpenEndedAnswerTable,
		closeEndedAnswerTable: sqlmetaimpl.CloseEndedAnswerTable,
	}
}
