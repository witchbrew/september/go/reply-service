package inmemory

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/september/go/reply-service/model/answer"
	"testing"
)

func TestOpenEndedCreateGetDelete(t *testing.T) {
	r := NewAnswerRepository()
	a := answer.OpenEnded("questionID", "All is fine")
	err := r.Create(a)
	require.Nil(t, err)
	require.True(t, a.ID().IsInitialized())
	defer func() {
		err := r.Delete(a.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(a.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	a2, err := r.Get(a.ID())
	require.Nil(t, err)
	require.NotNil(t, a2)
	require.Equal(t, a, a2)
}

func TestCloseEndedCreateGetDelete(t *testing.T) {
	r := NewAnswerRepository()
	a := answer.CloseEnded("questionID", 2)
	err := r.Create(a)
	require.Nil(t, err)
	require.True(t, a.ID().IsInitialized())
	defer func() {
		err := r.Delete(a.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(a.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	a2, err := r.Get(a.ID())
	require.Nil(t, err)
	require.NotNil(t, a2)
	require.Equal(t, a, a2)
}
