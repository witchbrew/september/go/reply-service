package inmemory

import (
	"github.com/pkg/errors"
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/september/go/reply-service/model/answer"
	"gitlab.com/witchbrew/september/go/reply-service/repository"
)

type answerRepository struct {
	currentID uint64
	storage   map[uint64]*answer.Answer
}

func (r *answerRepository) Create(a *answer.Answer) error {
	if a.ID().IsInitialized() {
		return errors.Errorf("question ID is already initialized to %v", a.ID().Value)
	}
	r.currentID += 1
	a.ID().Value = r.currentID
	r.storage[r.currentID] = a
	return nil
}

func (r *answerRepository) Get(i *id.ID) (*answer.Answer, error) {
	if !i.IsInitialized() {
		return nil, id.ErrNotInitialized
	}
	key := i.Value.(uint64)
	q, ok := r.storage[key]
	if ok {
		return q, nil
	}
	return nil, nil
}

func (r *answerRepository) Delete(i *id.ID) error {
	if !i.IsInitialized() {
		return id.ErrNotInitialized
	}
	key := i.Value.(uint64)
	delete(r.storage, key)
	return nil
}

func NewAnswerRepository() repository.Answer {
	return &answerRepository{
		currentID: uint64(0),
		storage:   map[uint64]*answer.Answer{},
	}
}
