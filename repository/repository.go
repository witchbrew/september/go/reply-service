package repository

import (
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/september/go/reply-service/model/answer"
)

type Answer interface {
	Create(*answer.Answer) error
	Get(*id.ID) (*answer.Answer, error)
	Delete(*id.ID) error
}
