package service

import (
	"gitlab.com/witchbrew/september/go/reply-service/service"
)

type questionMetaDataAccess struct {
	storage map[string]*service.QuestionMetaDTO
}

func (da *questionMetaDataAccess) Get(id string) (*service.QuestionMetaDTO, error) {
	return da.storage[id], nil
}

func NewQuestionMetaDataAccess(dtos ...*service.QuestionMetaDTO) service.QuestionMetaDataAccess {
	qmda := &questionMetaDataAccess{
		storage: make(map[string]*service.QuestionMetaDTO, len(dtos)),
	}
	for _, dto := range dtos {
		qmda.storage[dto.ID] = dto
	}
	return qmda
}
