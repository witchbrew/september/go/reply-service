package service

import (
	"fmt"
	"gitlab.com/witchbrew/go/errorhandling"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/september/go/reply-service/model/answer"
	"gitlab.com/witchbrew/september/go/reply-service/repository"
)

type Answer struct {
	idConverter            idconv.Converter
	questionMetaDataAccess QuestionMetaDataAccess
	repository             repository.Answer
}

type AnswerDTO struct {
	QuestionID string
	ID         string
	Text       string
	Options    []int
}

func (a *AnswerDTO) HasOptions() bool {
	return a.Options != nil
}

func (s *Answer) getQuestionMeta(questionID string) (*QuestionMetaDTO, error) {
	questionMetaDTO, err := s.questionMetaDataAccess.Get(questionID)
	if err != nil {
		return nil, err
	}
	if questionMetaDTO == nil {
		return nil, errorhandling.NotFoundErrorf(`question with ID="%s" does not exist`, questionID)
	}
	return questionMetaDTO, nil
}

func (s *Answer) CreateOpenEnded(questionID string, text string) (*AnswerDTO, error) {
	questionMetaDTO, err := s.getQuestionMeta(questionID)
	if err != nil {
		return nil, err
	}
	if questionMetaDTO.Options > 0 {
		return nil, errorhandling.BadInputErrorf("cannot create open ended answer for close ended question")
	}
	a := answer.OpenEnded(questionID, text)
	err = s.repository.Create(a)
	if err != nil {
		return nil, err
	}
	return &AnswerDTO{
		QuestionID: questionID,
		ID:         s.idConverter.ToString(a.ID()),
		Text:       text,
		Options:    nil,
	}, nil
}

func (s *Answer) CreateCloseEnded(questionID string, options ...int) (*AnswerDTO, error) {
	questionMetaDTO, err := s.getQuestionMeta(questionID)
	if err != nil {
		return nil, err
	}
	if questionMetaDTO.Options == 0 {
		return nil, errorhandling.BadInputErrorf("cannot create close ended answer for open ended question")
	}
	if len(options) < 1 {
		return nil, errorhandling.BadInputErrorf("at least one option must be provided")
	}
	if len(options) > questionMetaDTO.Options {
		return nil, errorhandling.BadInputErrorf("answer has more options than question")
	}
	var maxOption int
	for index, option := range options {
		if index == 0 || maxOption < option {
			maxOption = option
		}
	}
	if maxOption >= questionMetaDTO.Options {
		m := maxOption
		t := questionMetaDTO.Options
		msg := fmt.Sprintf("max option index (%d) is out of bounds for total number of options (%d)", m, t)
		return nil, errorhandling.BadInputErrorf(msg)
	}
	a := answer.CloseEnded(questionID, options...)
	err = s.repository.Create(a)
	if err != nil {
		return nil, err
	}
	return &AnswerDTO{
		QuestionID: questionID,
		ID:         s.idConverter.ToString(a.ID()),
		Text:       "",
		Options:    a.Options(),
	}, nil
}

func (s *Answer) Get(answerID string) (*AnswerDTO, error) {
	i, err := s.idConverter.FromString(answerID)
	if err != nil {
		return nil, errorhandling.BadInputError(err.Error())
	}
	a, err := s.repository.Get(i)
	if err != nil {
		return nil, err
	}
	if a == nil {
		return nil, errorhandling.NotFoundErrorf(`answer with ID="%s" does not exist`, answerID)
	}
	dto := &AnswerDTO{
		QuestionID: a.QuestionID(),
		ID:         answerID,
		Text:       "",
		Options:    nil,
	}
	if a.HasOptions() {
		dto.Options = a.Options()
	} else {
		dto.Text = a.Text()
	}
	return dto, nil
}

func (s *Answer) Delete(answerID string) error {
	i, err := s.idConverter.FromString(answerID)
	if err != nil {
		return errorhandling.BadInputError(err.Error())
	}
	err = s.repository.Delete(i)
	if err != nil {
		return err
	}
	return nil
}

func NewAnswer(idC idconv.Converter, qmDA QuestionMetaDataAccess, repo repository.Answer) *Answer {
	return &Answer{
		idConverter:            idC,
		questionMetaDataAccess: qmDA,
		repository:             repo,
	}
}
