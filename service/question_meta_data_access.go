package service

type QuestionMetaDTO struct {
	ID      string
	Options int
}

type QuestionMetaDataAccess interface {
	Get(id string) (*QuestionMetaDTO, error)
}
