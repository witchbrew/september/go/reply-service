package grpc

import (
	"context"
	"gitlab.com/witchbrew/september/go/askgrpc"
	"gitlab.com/witchbrew/september/go/reply-service/service"
)

type questionMetaDataAccess struct {
	client askgrpc.AskClient
}

func (q *questionMetaDataAccess) Get(id string) (*service.QuestionMetaDTO, error) {
	qm, err := q.client.GetMeta(context.Background(), &askgrpc.AskIDWrapper{ID: id})
	if err != nil {
		return nil, err
	}
	return &service.QuestionMetaDTO{
		ID:      id,
		Options: int(qm.Options),
	}, nil
}

func NewQuestionMetaDataAccess(client askgrpc.AskClient) service.QuestionMetaDataAccess {
	return &questionMetaDataAccess{client: client}
}
