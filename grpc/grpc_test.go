package grpc

import (
	"context"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/errorhandling"
	"gitlab.com/witchbrew/go/grpcutils/grpcerrors"
	"gitlab.com/witchbrew/go/idutils/idconv"
	repoInMemory "gitlab.com/witchbrew/september/go/reply-service/repository/inmemory"
	"gitlab.com/witchbrew/september/go/reply-service/service"
	serviceInMemory "gitlab.com/witchbrew/september/go/reply-service/service/inmemory"
	"gitlab.com/witchbrew/september/go/replygrpc"
	"testing"
)

func TestCreateGetDelete_OpenEnded(t *testing.T) {
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(&service.QuestionMetaDTO{
		ID:      "questionID",
		Options: 0,
	})
	gRPCServer := NewTestServer(t, service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository()))
	defer gRPCServer.Stop()
	gRPCClient := NewTestClient(t)
	w, err := gRPCClient.Create(context.Background(), &replygrpc.CreateAnswer{
		QuestionID: "questionID",
		Text:       "All is fine",
	})
	require.Nil(t, err)
	require.Equal(t, "1", w.ID)
	defer func() {
		_, err := gRPCClient.Delete(context.Background(), &replygrpc.ReplyIDWrapper{ID: "1"})
		require.Nil(t, err)
		a, err := gRPCClient.Get(context.Background(), &replygrpc.ReplyIDWrapper{ID: "1"})
		require.NotNil(t, err)
		require.Nil(t, a)
		hErr := grpcerrors.ToHandledError(err)
		require.Equal(t, errorhandling.NotFound, hErr.Type)
		require.Contains(t, hErr.Message, `answer with ID="1" does not exist`)
	}()
	a, err := gRPCClient.Get(context.Background(), &replygrpc.ReplyIDWrapper{ID: "1"})
	require.Nil(t, err)
	require.Equal(t, "1", a.ID)
	require.Equal(t, "questionID", a.QuestionID)
	require.Equal(t, "All is fine", a.Text)
}

func TestCreateGetDelete_CloseEnded(t *testing.T) {
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(&service.QuestionMetaDTO{
		ID:      "questionID",
		Options: 4,
	})
	gRPCServer := NewTestServer(t, service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository()))
	defer gRPCServer.Stop()
	gRPCClient := NewTestClient(t)
	w, err := gRPCClient.Create(context.Background(), &replygrpc.CreateAnswer{
		QuestionID: "questionID",
		Options: []int32{1, 3},
	})
	require.Nil(t, err)
	require.Equal(t, "1", w.ID)
	defer func() {
		_, err := gRPCClient.Delete(context.Background(), &replygrpc.ReplyIDWrapper{ID: "1"})
		require.Nil(t, err)
		a, err := gRPCClient.Get(context.Background(), &replygrpc.ReplyIDWrapper{ID: "1"})
		require.NotNil(t, err)
		require.Nil(t, a)
		hErr := grpcerrors.ToHandledError(err)
		require.Equal(t, errorhandling.NotFound, hErr.Type)
		require.Contains(t, hErr.Message, `answer with ID="1" does not exist`)
	}()
	a, err := gRPCClient.Get(context.Background(), &replygrpc.ReplyIDWrapper{ID: "1"})
	require.Nil(t, err)
	require.Equal(t, "1", a.ID)
	require.Equal(t, "questionID", a.QuestionID)
	require.Equal(t, []int32{1, 3}, a.Options)
}

func TestCreate_BothTextAndOptions(t *testing.T) {
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(&service.QuestionMetaDTO{
		ID:      "questionID",
		Options: 4,
	})
	gRPCServer := NewTestServer(t, service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository()))
	defer gRPCServer.Stop()
	gRPCClient := NewTestClient(t)
	w, err := gRPCClient.Create(context.Background(), &replygrpc.CreateAnswer{
		QuestionID: "questionID",
		Text: "All is fine",
		Options: []int32{1, 3},
	})
	require.NotNil(t, err)
	require.Nil(t, w)
	hErr := grpcerrors.ToHandledError(err)
	require.Equal(t, errorhandling.BadInput, hErr.Type)
	require.Contains(t, hErr.Message, "when providing options text must be empty")
}