package grpc

import (
	"context"
	"gitlab.com/witchbrew/go/grpcutils/grpcerrors"
	"gitlab.com/witchbrew/go/grpcutils/server_interceptors"
	"gitlab.com/witchbrew/september/go/reply-service/service"
	"gitlab.com/witchbrew/september/go/replygrpc"
	"google.golang.org/grpc"
)

type replyServer struct {
	replygrpc.UnimplementedReplyServer
	answerService *service.Answer
}

func (s *replyServer) Create(ctx context.Context, a *replygrpc.CreateAnswer) (*replygrpc.ReplyIDWrapper, error) {
	if len(a.Options) > 0 && a.Text != "" {
		return nil, grpcerrors.InvalidArgumentf(`when providing options text must be empty`)
	}
	var aDTO *service.AnswerDTO
	var aErr error
	if len(a.Options) > 0 {
		options := make([]int, len(a.Options))
		for index, option := range a.Options {
			options[index] = int(option)
		}
		aDTO, aErr = s.answerService.CreateCloseEnded(a.QuestionID, options...)
	} else {
		aDTO, aErr = s.answerService.CreateOpenEnded(a.QuestionID, a.Text)
	}
	if aErr != nil {
		return nil, grpcerrors.Process(aErr)
	}
	return &replygrpc.ReplyIDWrapper{ID: aDTO.ID}, nil
}

func (s *replyServer) Get(ctx context.Context, w *replygrpc.ReplyIDWrapper) (*replygrpc.GetAnswer, error) {
	aDTO, err := s.answerService.Get(w.ID)
	if err != nil {
		return nil, grpcerrors.Process(err)
	}
	resp := &replygrpc.GetAnswer{
		ID:         aDTO.ID,
		QuestionID: aDTO.QuestionID,
		Text:       "",
		Options:    nil,
	}
	if aDTO.HasOptions() {
		options := make([]int32, len(aDTO.Options))
		for index, option := range aDTO.Options {
			options[index] = int32(option)
		}
		resp.Options = options
	} else {
		resp.Text = aDTO.Text
	}
	return resp, nil
}

func (s *replyServer) Delete(ctx context.Context, w *replygrpc.ReplyIDWrapper) (*replygrpc.ReplyEmpty, error) {
	err := s.answerService.Delete(w.ID)
	if err != nil {
		return nil, grpcerrors.Process(err)
	}
	return &replygrpc.ReplyEmpty{}, nil
}

func New(answerService *service.Answer) (*grpc.Server, error) {
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		server_interceptors.RequestID,
		server_interceptors.Zerolog,
		server_interceptors.ZerologRequestID,
		server_interceptors.ZerologLogServerInfo,
	))
	replygrpc.RegisterReplyServer(grpcServer, &replyServer{answerService: answerService})
	return grpcServer, nil
}
