package config

import (
	"gitlab.com/witchbrew/go/configutils"
	"gitlab.com/witchbrew/go/configutils/db_type_config"
	"gitlab.com/witchbrew/go/configutils/postgres_config"
	"os"
)

func NewFromEnv() (*Config, error) {
	httpPort, err := configutils.ParseStringPort(os.Getenv("HTTP_PORT"), 8080)
	if err != nil {
		return nil, err
	}
	gRPCPort, err := configutils.ParseStringPort(os.Getenv("GRPC_PORT"), 50051)
	if err != nil {
		return nil, err
	}
	dbType, err := db_type_config.FromEnv(db_type_config.Postgres)
	if err != nil {
		return nil, err
	}
	config := &Config{
		HTTPPort:           httpPort,
		GRPCPort:           gRPCPort,
		AskServiceGRPCHost: os.Getenv("ASK_SERVICE_GRPC_HOST"),
		DBType:             dbType,
		Postgres:           nil,
	}
	if dbType == db_type_config.Postgres {
		postgresConfig, err := postgres_config.NewFromEnv()
		if err != nil {
			return nil, err
		}
		config.Postgres = postgresConfig
	}
	return config, nil
}
