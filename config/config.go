package config

import (
	"gitlab.com/witchbrew/go/configutils"
	"gitlab.com/witchbrew/go/configutils/db_type_config"
	"gitlab.com/witchbrew/go/configutils/postgres_config"
)

type Config struct {
	HTTPPort           configutils.Port
	GRPCPort           configutils.Port
	AskServiceGRPCHost string
	DBType             db_type_config.DBType
	Postgres           *postgres_config.Config
}
