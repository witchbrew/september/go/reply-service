module gitlab.com/witchbrew/september/go/reply-service

go 1.15

require (
	github.com/lib/pq v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/witchbrew/go/configutils v0.0.0-20201014113634-307af4e4d739
	gitlab.com/witchbrew/go/errorhandling v0.0.0-20201011115201-3517a6876788
	gitlab.com/witchbrew/go/grpcutils v0.0.0-20201014110701-15f338272ca6
	gitlab.com/witchbrew/go/http v0.0.0-20201011115246-fe58e5f3c5ce
	gitlab.com/witchbrew/go/idutils v0.0.0-20201012135713-dea506f8a348
	gitlab.com/witchbrew/go/jsonschema v0.0.0-20201007113809-94945555a9a8
	gitlab.com/witchbrew/go/postgresutils v0.0.0-20201003224211-440ace86fd49
	gitlab.com/witchbrew/go/sqlmeta v0.0.0-20200814183640-57b148b29172
	gitlab.com/witchbrew/go/sqltemplate v0.0.0-20201003202731-5ff7ea0de46c
	gitlab.com/witchbrew/go/sqlutils v0.0.0-20201003202214-4bc16d2bb29f
	gitlab.com/witchbrew/september/go/askgrpc v0.0.0-20201014112112-4cbd5b03fc79
	gitlab.com/witchbrew/september/go/replygrpc v0.0.0-20201014112151-1426e01304a4
	google.golang.org/grpc v1.32.0
)
