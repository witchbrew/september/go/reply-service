FROM golang:1.15.2-alpine3.12

RUN apk update
RUN apk add git

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY . ./
RUN go build

FROM alpine:3.12
COPY --from=0 /app/reply-service .
EXPOSE 8080
EXPOSE 50051
ENTRYPOINT ["./reply-service"]