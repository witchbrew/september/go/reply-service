package postgres

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/idutils/id"
	"gitlab.com/witchbrew/go/sqlutils/sqltesting"
	"gitlab.com/witchbrew/september/go/reply-service/model/answer"
	"gitlab.com/witchbrew/september/go/reply-service/repository/postgres"
	"os"
	"testing"
)

func newDB(t *testing.T) *sql.DB {
	user := os.Getenv("TEST_PG_USERNAME")
	password := os.Getenv("TEST_PG_PASSWORD")
	host := os.Getenv("TEST_PG_HOST")
	database := os.Getenv("TEST_PG_DATABASE")
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		password,
		host,
		database,
	)
	db, err := sql.Open("postgres", connStr)
	require.Nil(t, err)
	return db

}

func TestOpenEndedNonExistentID(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewAnswerRepository(db)
	nonExistentID := id.New()
	nonExistentID.Value = 100500
	nonExistentQuestion, err := r.Get(nonExistentID)
	require.Nil(t, err)
	require.Nil(t, nonExistentQuestion)
}

func TestOpenEndedCreateGetDelete(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewAnswerRepository(db)
	a := answer.OpenEnded("questionID", "All is fine")
	err := r.Create(a)
	require.Nil(t, err)
	require.True(t, a.ID().IsInitialized())
	defer func() {
		err := r.Delete(a.ID())
		require.Nil(t, err)
		nonExistentA, err := r.Get(a.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentA)
	}()
	a2, err := r.Get(a.ID())
	require.Nil(t, err)
	require.NotNil(t, a2)
	require.Equal(t, a, a2)
}

func TestCloseEndedCreateGetDelete(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	r := postgres.NewAnswerRepository(db)
	a := answer.CloseEnded("questionID", 2)
	err := r.Create(a)
	require.Nil(t, err)
	require.True(t, a.ID().IsInitialized())
	defer func() {
		err := r.Delete(a.ID())
		require.Nil(t, err)
		nonExistentQ, err := r.Get(a.ID())
		require.Nil(t, err)
		require.Nil(t, nonExistentQ)
	}()
	a2, err := r.Get(a.ID())
	require.Nil(t, err)
	require.NotNil(t, a2)
	require.Equal(t, a, a2)
}
