package cmd

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/witchbrew/go/configutils/db_type_config"
	"gitlab.com/witchbrew/go/idutils/idconv"
	"gitlab.com/witchbrew/september/go/askgrpc"
	"gitlab.com/witchbrew/september/go/reply-service/api"
	"gitlab.com/witchbrew/september/go/reply-service/config"
	replyGRPC "gitlab.com/witchbrew/september/go/reply-service/grpc"
	"gitlab.com/witchbrew/september/go/reply-service/repository"
	"gitlab.com/witchbrew/september/go/reply-service/repository/inmemory"
	"gitlab.com/witchbrew/september/go/reply-service/repository/postgres"
	"gitlab.com/witchbrew/september/go/reply-service/service"
	questionMetaGRPC "gitlab.com/witchbrew/september/go/reply-service/service/grpc"
	"google.golang.org/grpc"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var appConfig *config.Config

func getConfig() (*config.Config, error) {
	if appConfig == nil {
		c, err := config.NewFromEnv()
		if err != nil {
			return nil, err
		}
		appConfig = c
	}
	return appConfig, nil
}

var rootCmd = &cobra.Command{
	Use: "reply-service",
	RunE: func(cmd *cobra.Command, args []string) error {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		conf, err := getConfig()
		if err != nil {
			return err
		}
		var idConverter idconv.Converter
		var questionRepository repository.Answer
		switch conf.DBType {
		case db_type_config.Postgres:
			db, err := sql.Open("postgres", conf.Postgres.ConnectionString())
			if err != nil {
				return err
			}
			defer func() {
				err := db.Close()
				if err != nil {
					panic(err)
				}
			}()
			questionRepository = postgres.NewAnswerRepository(db)
			idConverter = idconv.ConverterUint64
		case db_type_config.InMemory:
			questionRepository = inmemory.NewAnswerRepository()
			idConverter = idconv.ConverterUint64
		default:
			return errors.Errorf(`cannot create question repository for DB type "%s"`, conf.DBType)
		}
		clientConn, err := grpc.Dial(conf.AskServiceGRPCHost, grpc.WithInsecure())
		if err != nil {
			return err
		}
		askClient := askgrpc.NewAskClient(clientConn)
		qma := questionMetaGRPC.NewQuestionMetaDataAccess(askClient)
		answerService := service.NewAnswer(idConverter, qma, questionRepository)
		httpAPIHandler := api.New(answerService)
		gRPCListener, err := net.Listen("tcp", conf.GRPCPort.ToLocalAddress())
		if err != nil {
			return err
		}
		gRPCServer, err := replyGRPC.New(answerService)
		if err != nil {
			return err
		}
		go func() {
			err := http.ListenAndServe(conf.HTTPPort.ToLocalAddress(), httpAPIHandler)
			if err != nil {
				panic(err)
			}
		}()
		log.Logger.Info().Uint16("port", uint16(conf.HTTPPort)).Msg("serving HTTP")
		go func() {
			err := gRPCServer.Serve(gRPCListener)
			if err != nil {
				panic(err)
			}
		}()
		log.Logger.Info().Uint16("port", uint16(conf.GRPCPort)).Msg("serving gRPC")
		signalChannel := make(chan os.Signal)
		signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
		<-signalChannel
		return nil
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
