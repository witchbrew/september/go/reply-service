package sqlmeta

import "gitlab.com/witchbrew/go/sqlmeta"

type AnswerTable struct {
	*sqlmeta.Table
	Columns *AnswerColumns
}

type AnswerColumns struct {
	ID         string
	QuestionID string
}

type OpenEndedAnswerTable struct {
	*sqlmeta.Table
	Columns *OpenEndedAnswerColumns
}

type OpenEndedAnswerColumns struct {
	AnswerID string
	Text     string
}

type CloseEndedAnswerTable struct {
	*sqlmeta.Table
	Columns *CloseEndedAnswerColumns
}

type CloseEndedAnswerColumns struct {
	AnswerID    string
	OptionIndex string
}
