package answer

import (
	"gitlab.com/witchbrew/go/idutils/id"
)

type Answer struct {
	id         *id.ID
	questionID string
	text       string
	options    []int
}

func (a *Answer) ID() *id.ID {
	return a.id
}

func (a *Answer) QuestionID() string {
	return a.questionID
}

func (a *Answer) HasOptions() bool {
	return a.options != nil
}

func (a *Answer) Text() string {
	if a.HasOptions() {
		panic("answer is close ended")
	}
	return a.text
}

func (a *Answer) Options() []int {
	if !a.HasOptions() {
		panic("answer is open ended")
	}
	options := make([]int, len(a.options))
	for index, option := range a.options {
		options[index] = option
	}
	return options
}

func OpenEnded(questionID string, text string) *Answer {
	return &Answer{
		id:         id.New(),
		questionID: questionID,
		text:       text,
		options:    nil,
	}
}

func CloseEnded(questionID string, options ...int) *Answer {
	return &Answer{
		id:         id.New(),
		questionID: questionID,
		text:       "",
		options:    options,
	}
}
