package api

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/http/httptestclient"
	"gitlab.com/witchbrew/go/idutils/idconv"
	repoInMemory "gitlab.com/witchbrew/september/go/reply-service/repository/inmemory"
	"gitlab.com/witchbrew/september/go/reply-service/service"
	serviceInMemory "gitlab.com/witchbrew/september/go/reply-service/service/inmemory"
	"net/http"
	"testing"
)

func TestCreateGetDelete_OpenEnded(t *testing.T) {
	qm := &service.QuestionMetaDTO{ID: "questionID"}
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(qm)
	s := service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository())
	c := httptestclient.NewJSON(t, New(s))
	m := map[string]interface{}{
		"questionID": qm.ID,
		"text":       "All is fine",
	}
	resp := c.Post("/").Body(m).Send()
	require.Equal(t, map[string]interface{}{"ID": "1"}, resp.Body)
	require.Equal(t, http.StatusCreated, resp.StatusCode)
	require.Equal(t, "application/json", resp.Headers.Get("Content-Type"))
	defer func() {
		resp = c.Delete("/1").Send()
		require.Equal(t, http.StatusNoContent, resp.StatusCode)
		resp = c.Get("/1").Send()
		require.Equal(t, http.StatusNotFound, resp.StatusCode)
	}()
	resp = c.Get("/1").Send()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	m["ID"] = "1"
	require.Equal(t, m, resp.Body)
}

func TestCreateGetDelete_CloseEnded(t *testing.T) {
	qm := &service.QuestionMetaDTO{ID: "questionID", Options: 3}
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(qm)
	s := service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository())
	c := httptestclient.NewJSON(t, New(s))
	m := map[string]interface{}{
		"questionID": "questionID",
		"options":    []interface{}{2.0},
	}
	resp := c.Post("/").Body(m).Send()
	require.Equal(t, map[string]interface{}{"ID": "1"}, resp.Body)
	require.Equal(t, http.StatusCreated, resp.StatusCode)
	require.Equal(t, "application/json", resp.Headers.Get("Content-Type"))
	require.Equal(t, map[string]interface{}{"ID": "1"}, resp.Body)
	defer func() {
		resp = c.Delete("/1").Send()
		require.Equal(t, http.StatusNoContent, resp.StatusCode)
		resp = c.Get("/1").Send()
		require.Equal(t, http.StatusNotFound, resp.StatusCode)
	}()
	resp = c.Get("/1").Send()
	require.Equal(t, http.StatusOK, resp.StatusCode)
	m["ID"] = "1"
	require.Equal(t, m, resp.Body)
}

func TestGet_BadID(t *testing.T) {
	qm := &service.QuestionMetaDTO{ID: "questionID"}
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(qm)
	s := service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository())
	c := httptestclient.NewJSON(t, New(s))
	resp := c.Get("/bad-ID").Send()
	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
	require.Equal(t, map[string]interface{}{
		"type":    "informative",
		"message": `bad uint64 string: "bad-ID"`,
	}, resp.Body)
}

func TestCreate_BothTextAndOptions(t *testing.T) {
	qm := &service.QuestionMetaDTO{ID: "questionID"}
	qmDA := serviceInMemory.NewQuestionMetaDataAccess(qm)
	s := service.NewAnswer(idconv.ConverterUint64, qmDA, repoInMemory.NewAnswerRepository())
	c := httptestclient.NewJSON(t, New(s))
	m := map[string]interface{}{
		"questionID": "questionID",
		"text":       "All is fine",
		"options":    []interface{}{2},
	}
	resp := c.Post("/").Body(m).Send()
	require.Equal(t, map[string]interface{}{
		"message": "Bad answer object",
		"type":    "validation",
		"values":  map[string]interface{}{"@root": "either text or options must be present"},
	}, resp.Body)
	require.Equal(t, http.StatusBadRequest, resp.StatusCode)
	require.Equal(t, "application/json", resp.Headers.Get("Content-Type"))
}
