package api

import (
	"gitlab.com/witchbrew/go/http/httphandler"
	"gitlab.com/witchbrew/go/http/httprouter"
	"gitlab.com/witchbrew/go/jsonschema"
	"gitlab.com/witchbrew/september/go/reply-service/service"
	"net/http"
)

type (
	sch = jsonschema.Sch
	obj = jsonschema.Obj
	arr = jsonschema.Arr
)

var CreateAnswerSch = sch{
	"type":                 "object",
	"additionalProperties": false,
	"properties": obj{
		"questionID": obj{
			"type": "string",
		},
		"text": obj{
			"type": "string",
		},
		"options": obj{
			"type": "array",
			"items": obj{
				"type": "integer",
			},
			"minItems": 1,
		},
	},
	"required": arr{"questionID"},
}
var createAnswerSchema = CreateAnswerSch.MustCompile()

func convertValidationError(validationErr *jsonschema.ValidationError, message string) *httphandler.Error {
	values := httphandler.ErrorValues{}
	for _, pathErr := range validationErr.Errors {
		values[pathErr.StringPath(".")] = pathErr.Message
	}
	return &httphandler.Error{
		Type:    "validation",
		Message: message,
		Values:  values,
	}
}

func New(answerService *service.Answer) http.Handler {
	r := httprouter.NewJSON()
	r.Post("/", func(context *httphandler.HandlerContext) {
		validationErr := createAnswerSchema.Validate(context.Request.Body())
		if validationErr != nil {
			httpErr := convertValidationError(validationErr, "Bad answer object")
			context.Response.Error(http.StatusBadRequest, httpErr)
			return
		}
		answerDTO := context.Request.Body().(map[string]interface{})
		questionID := answerDTO["questionID"].(string)
		answerTextDTO, textIsPresent := answerDTO["text"]
		_, optionsArePresent := answerDTO["options"]
		if textIsPresent && optionsArePresent || !textIsPresent && !optionsArePresent {
			httpErr := convertValidationError(jsonschema.NewValidationError(
				jsonschema.ErrorForPath().WithMsg("either text or options must be present"),
			), "Bad answer object")
			context.Response.Error(http.StatusBadRequest, httpErr)
			return
		}
		var a *service.AnswerDTO
		var aErr error
		if optionsArePresent {
			optionsDTO := answerDTO["options"].([]interface{})
			options := make([]int, len(optionsDTO))
			for index, option := range optionsDTO {
				options[index] = int(option.(float64))
			}
			a, aErr = answerService.CreateCloseEnded(questionID, options...)
		} else {
			a, aErr = answerService.CreateOpenEnded(questionID, answerTextDTO.(string))
		}
		if aErr != nil {
			context.Response.ProcessError(aErr)
			return
		}
		context.Response.Body(http.StatusCreated, obj{
			"ID": a.ID,
		})
	})
	r.Get("/{ID}", func(context *httphandler.HandlerContext) {
		i := context.Request.URLParam("ID")
		a, err := answerService.Get(i)
		if err != nil {
			context.Response.ProcessError(err)
			return
		}
		body := obj{
			"ID":         a.ID,
			"questionID": a.QuestionID,
		}
		if a.HasOptions() {
			bodyOptions := make(arr, len(a.Options))
			for index, option := range a.Options {
				bodyOptions[index] = option
			}
			body["options"] = bodyOptions
		} else {
			body["text"] = a.Text
		}
		context.Response.Body(http.StatusOK, body)
	})
	r.Delete("/{ID}", func(context *httphandler.HandlerContext) {
		i := context.Request.URLParam("ID")
		err := answerService.Delete(i)
		if err != nil {
			context.Response.ProcessError(err)
			return
		}
		context.Response.Empty(http.StatusNoContent)
	})
	return r
}
